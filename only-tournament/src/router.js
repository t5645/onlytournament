import Vue from 'vue'
import Router from 'vue-router'
import Home from './App/components/home/Home.vue'
import NotFound from './App/components/notFound/NotFound.vue'
import Contact from './App/components/contact/Contact.vue'
import Account from './App/components/account/Account.vue'
import Tournament from './App/components/tournament/Tournament.vue'
import Player from './App/components/player/Player.vue'
import Organizer from './App/components/organizer/Organizer.vue'

Vue.use(Router)

export default new Router({
    mode:'history',
    routes: [
        {
            path: '/',
            redirect: {name :'home'}
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path:'/contact',
            name:'contact',
            component: Contact
        },
        {
            path: '*',
            redirect:{name: 'not-found'}
        },
        {
            path: '/not-found',
            name: 'not-found',
            component: NotFound
        },
        {
            path:'/account',
            name:'account',
            component: Account
        },
        {
            path:'/tournament',
            name:'tournament',
            component: Tournament
        },
        {
            path:'/player',
            name:'player',
            component: Player
        },
        {
            path:'/organizer',
            name:'organizer',
            component: Organizer
        }
    ]
})